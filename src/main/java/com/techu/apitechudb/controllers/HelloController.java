package com.techu.apitechudb.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public ResponseEntity index() {
        System.out.println("Hello Controller");
        return new ResponseEntity<>("Hola Mundo desde API Tech U!", HttpStatus.OK);
        //return ResponseEntity.ok("Hola Mundo");
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "TechU") String name){
        return "Hola Controller: "+name;
    }

    //Ejemplo llamada: http://localhost:8080/sum?val1=70&val2=50
    @RequestMapping("/sum")
    public String sum(@RequestParam(value = "val1", defaultValue = "0") int val1, @RequestParam(value = "val2", defaultValue = "0") int val2){
        return "Hola Sum: "+val1+" + "+val2+ " = "+(val1+val2);
    }
}
