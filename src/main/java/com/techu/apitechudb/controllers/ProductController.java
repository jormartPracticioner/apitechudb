package com.techu.apitechudb.controllers;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {

        System.out.println("<Controller> getProducts");
        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    /*@GetMapping("/product/{id}")
        public ProductModel getProduct(@PathVariable String id){

            System.out.println("<Controller> getProduct");
            System.out.println("<Controller> el id es "+ id);

            ProductModel result = new ProductModel();

            for (ProductModel product: ApitechudbApplication.productModels){

                if(product.getId().equals(id)){
                    result= product;
                }
            }

            // Obtener listado de productos
            // Abrir una conexion a BD
            // Gestionar conexion a BD
            // Ejecutar la consulta
            // Procesar los resultados obtenidos
            // Ya con el listado de productos, mostrar al usuario / devolver resultado



            return result;

        } */

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){

        System.out.println("<Controller> addProduct");
        System.out.println("<Controller> El id del producto que se va a crear es "+product.getId());
        System.out.println("<Controller> La descripción del producto que se va a crear es "+product.getDesc());
        System.out.println("<Controller> El precio del producto que se va a crear es "+product.getPrice());

        return new ResponseEntity<>(this.productService.add(product),HttpStatus.CREATED);

    }

    @GetMapping("products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){

        System.out.println("<Controller> getProductById");
        System.out.println("<Controller> El id del producto que vamos a obtener es "+id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent()? result.get() : "Producto no encontrado"
                , result.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @PutMapping("products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id){

        System.out.println("<Controller> updateProduct");
        System.out.println("<Controller> El id del producto que vamos a actualizar es "+id);
        System.out.println("<Controller> El id (body) del producto que vamos a actualizar es "+product.getId());
        System.out.println("<Controller> La descripción del producto que se va a actualizar  es "+product.getDesc());
        System.out.println("<Controller> El precio del producto que se va a actualizar es "+product.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if(productToUpdate.isPresent()){

            System.out.println("<Controller> Producto para actualizar encontrad, actualizando");

            this.productService.update(product);
        }

        return new ResponseEntity<>(product,
                productToUpdate.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }


    @DeleteMapping("products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){

        System.out.println("<Controller> deleteProduct");
        System.out.println("<Controller> El id del producto que vamos a borrar es "+id);

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no borrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


}
