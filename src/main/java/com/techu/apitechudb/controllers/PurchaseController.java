package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {

        System.out.println("<Controller> getPurchases");
        return new ResponseEntity<>(this.purchaseService.findAll(), HttpStatus.OK);
    }

    @GetMapping("purchases/{id}")
    public ResponseEntity<Object> getPurchaseById(@PathVariable String id){

        System.out.println("<Controller> getPurchaseById");
        System.out.println("<Controller> El id de la compra que vamos a obtener es "+id);

        Optional<PurchaseModel> result = this.purchaseService.findById(id);

        return new ResponseEntity<>(
                result.isPresent()? result.get() : "Compra no encontrada"
                , result.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @PostMapping("/purchases")
    public ResponseEntity<String> addPurchase(@RequestBody PurchaseModel purchase){

        System.out.println("<Controller> addPurchase");
        System.out.println("<Controller> El id de la compra que se va a crear es "+purchase.getId());
        System.out.println("<Controller> El userId de la compra que se va a crear es "+purchase.getUserId());
        System.out.println("<Controller> El total de la compra que se va a crear es "+purchase.getAmount());
        System.out.println("<Controller> El listado de items de la compra que se va a crear es "+purchase.getPurchaseItems());

        PurchaseServiceResponse result = this.purchaseService.add(purchase);
        return new ResponseEntity<>(result.getMsg(), result.getResponseHttpStatusCode());
    }

    @DeleteMapping("purchases/{id}")
    public ResponseEntity<String> deletePurchase(@PathVariable String id){

        System.out.println("<Controller> deletePurchase");
        System.out.println("<Controller> El id de la compra que vamos a borrar es "+id);

        boolean deletePurchase = this.purchaseService.delete(id);

        return new ResponseEntity<>(
                deletePurchase ? "Compra borrada" : "Compra no borrada",
                deletePurchase ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/purchases")
    public ResponseEntity<String> deleteAllPurchases() {

        System.out.println("<Controller> deleteAllPurchases");
        List<PurchaseModel> purchases = this.purchaseService.findAll();

        Iterator it = purchases.iterator();
        while(it.hasNext())
        {
            PurchaseModel purchase = (PurchaseModel) it.next();
            this.purchaseService.delete(purchase.getId());
        }

        return new ResponseEntity<>("Compras borradas", HttpStatus.OK);
    }
}
