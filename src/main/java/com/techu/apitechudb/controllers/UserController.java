package com.techu.apitechudb.controllers;

import com.techu.apitechudb.services.UserService;
import com.techu.apitechudb.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class UserController {

    @Autowired
    UserService userService;

    //Obtener todos los usuarios, pudiendo ordenarlos por su edad (vale cualquier dirección de la ordenación).
    // NOTA - Utilizar un parámetro de Query String opcional para pedir ordenar en base a la edad.
    // PISTA - Buscar cómo ordenar en Spring Data.
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value="$orderBy",required = false) String orderBy) {

        if(orderBy == null)
            System.out.println("<Controller> getUsers");
        else
            System.out.println("<Controller> getUsers order by "+orderBy);

        return new ResponseEntity<>(this.userService.findAll(orderBy), HttpStatus.OK);
    }

    /*
    @GetMapping("/users/order/{order}")
    public ResponseEntity<List<UserModel>> getUsers(@PathVariable String order) {

        System.out.println("<Controller> getUsersOrder by "+order);
        return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);
    }
    */

    //Obtener un usuario en base a su id.
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){

        System.out.println("<Controller> getUserById");
        System.out.println("<Controller> El id del usuario que vamos a obtener es "+id);

        Optional<UserModel> result = this.userService.findById(id);

        //Si no pones el get no se devuelve el UserModel por ser Optional
        return new ResponseEntity<>(
                result.isPresent()? result.get() : "Usuario no encontrado"
                , result.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    //Crear un usuario
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){

        System.out.println("<Controller> addUser");
        System.out.println("<Controller> El id del usuario que se va a crear es "+user.getId());
        System.out.println("<Controller> El nombre del usuario que se va a crear es "+user.getName());
        System.out.println("<Controller> La edad del usuario que se va a crear es "+user.getAge());

        return new ResponseEntity<>(this.userService.add(user),HttpStatus.CREATED);
    }

    //Actualizar completamente un usuario (menos la id) en base a su id.
    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id){

        System.out.println("<Controller> updateUser");
        System.out.println("<Controller> El id del usuario que vamos a actualizar es "+id);
        System.out.println("<Controller> El id (body) del usuario que vamos a actualizar es "+user.getId());
        System.out.println("<Controller> El nombre del usuario que se va a actualizar  es "+user.getName());
        System.out.println("<Controller> La edad del usuario que se va a actualizar es "+user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if(userToUpdate.isPresent()){

            System.out.println("<Controller> Usuario para actualizar encontrado, actualizando");
            //Forzamos a que no actualice el id recibido por Body, reseteamos el dato
            user.setId(id);
            this.userService.update(user);
        }

        return new ResponseEntity<>(user,
                userToUpdate.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    //Actualizar completamente un usuario (menos la id) en base a su id.
    //Nueva versión del método anterir pero utilizando Patch para evitar settear el id
    @PatchMapping("/users/{id}")
    public ResponseEntity<UserModel> partialUpdateUser(@RequestBody UserModel user, @PathVariable String id) {

        System.out.println("<Controller> partialUpdateUser");
        System.out.println("<Controller> El id del usuario que vamos a actualizar es "+id);
        System.out.println("<Controller> El nombre del usuario que se va a actualizar  es "+user.getName());
        System.out.println("<Controller> La edad del usuario que se va a actualizar es "+user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if(userToUpdate.isPresent()){

            System.out.println("<Controller> Usuario para actualizar encontrado, actualizando");
            if(user.getName() != null)
            {
                userToUpdate.get().setName(user.getName());
            }
            if(user.getAge() != null)
            {
                userToUpdate.get().setAge(user.getAge());
            }
            this.userService.update(userToUpdate.get());
        }

        return new ResponseEntity<>(userToUpdate.get(),
                userToUpdate.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    //Borrar un usuario en base a su id.
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){

        System.out.println("<Controller> deleteUser");
        System.out.println("<Controller> El id del usuario que vamos a borrar es "+id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
