package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "purchases")  //nombre de tabla
public class PurchaseModel {

    @Id  //como la PK
    private String id;
    private String userId;
    private Float amount;
    private Map<String, Integer> purchaseItems;  //par de valores: idProducto + cantidad comprada

    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userId, Float amount, Map<String, Integer> purchaseItems) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public Float getAmount() {
        return amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return purchaseItems;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
