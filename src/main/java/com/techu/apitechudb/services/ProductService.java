package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        //No hay instanciación del atributo. Esto lo hace el Autowired
        //Se hace una inversión del control.
        System.out.println("<Service> findAll");
        return this.productRepository.findAll();

    }

    public ProductModel add(ProductModel product){
        System.out.println("<Service> add");
        return this.productRepository.save(product);
    }

    public Optional<ProductModel> findById(String id){

        System.out.println("<Service> findById");

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel product){

        System.out.println("<Service> update");

        return this.productRepository.save(product);
    }

    public boolean delete(String id){

        System.out.println("<Service> delete");

        boolean result = false;

        if(this.productRepository.findById(id).isPresent() == true) {
            System.out.println("<Service> Producto encontrado, borrando");
            this.productRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
