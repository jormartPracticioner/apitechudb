package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.PurchaseRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;

    public List<PurchaseModel> findAll() {
        System.out.println("<Service> findAll");
        return this.purchaseRepository.findAll();
    }

    public PurchaseServiceResponse add(PurchaseModel purchase){
        System.out.println("<Service> add");

        PurchaseServiceResponse result = new PurchaseServiceResponse();
        result.setPurchase(purchase);

        float totalAmount = 0f;

        // Comprobar que la id de usuario se corresponda con un usuario en BD.
        if(this.userRepository.findById(purchase.getUserId()).isPresent() == true) {
            System.out.println("<Service> OK: Usuario existente");
        }else{
            System.out.println("<Service> ERROR: Usuario no encontrado: "+purchase.getUserId());
            result.setMsg("ERROR: Usuario no encontrado: "+purchase.getUserId());
            result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
            return result;
        }

        // Comprobar que no haya una compra con esa id en BD.
        if(this.purchaseRepository.findById(purchase.getId()).isPresent() == true) {
            System.out.println("<Service> ERROR: Compra existente");
            result.setMsg("ERROR: Compra existente: "+purchase.getId());
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
            return result;
        }else{
            System.out.println("<Service> OK: Compra nueva");
        }

        /* Alternativa al Iterator:
        for(Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()){
            if(this.productRepository.findById(purchaseItem.getKey()).isEmpty())
            {
                System.out.println("<Service> ERROR: Producto no existente: "+purchaseItem.getKey());
            }
        }*/

        // Comprobar que todos los productos están en BD.
        Iterator it = purchase.getPurchaseItems().keySet().iterator();
        while(it.hasNext()) {
            String key = (String) it.next();
            Integer amount = purchase.getPurchaseItems().get(key);
            System.out.println("<Service> Producto: " + key + " -> Cantidad: " + amount);
            if (this.productRepository.findById(key).isPresent() == true) {
                System.out.println("<Service> OK: Producto encontrado");
                Float price = this.productRepository.findById(key).get().getPrice();
                System.out.println("<Service> Price: "+price);
                System.out.println("<Service> Subtotal: "+(price*amount));
                totalAmount+= price*amount;
            } else {
                System.out.println("<Service> ERROR: Producto no existente");
                result.setMsg("ERROR: Producto no encontrado:"+key);
                result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
                return result;
            }
        }

        // Una vez tengamos el OK, calcular total de la compra en base al precio
        //   almacenado en BD de cada uno de los productos.
        System.out.println("<Service> TotalAmount = "+totalAmount);
        System.out.println("<Service> PURCHASE OK! Insertando");
        purchase.setAmount(totalAmount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;
    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("<Service> findById");
        return this.purchaseRepository.findById(id);
    }

    public PurchaseModel update(PurchaseModel purchase){
        System.out.println("<Service> update");
        return this.purchaseRepository.save(purchase);
    }

    public boolean delete(String id){
        System.out.println("<Service> delete");
        boolean result = false;

        if(this.purchaseRepository.findById(id).isPresent() == true) {
            System.out.println("<Service> Compra encontrada ("+id+"), borrando");
            this.purchaseRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}
