package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(String orderBy) {
        if(orderBy == null) {
            System.out.println("<Service> findAll");
            return this.userRepository.findAll();
        }
        else {
            System.out.println("<Service> findAll order by " + orderBy);
            return this.userRepository.findAll(Sort.by(Sort.Direction.ASC,orderBy));
        }
    }

    public UserModel add(UserModel user){
        System.out.println("<Service> add");
        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id){
        System.out.println("<Service> findById");
        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel user){
        System.out.println("<Service> update");
        return this.userRepository.save(user);
    }

    public boolean delete(String id){
        System.out.println("<Service> delete");
        boolean result = false;

        if(this.userRepository.findById(id).isPresent() == true) {
            System.out.println("<Service> Usuario encontrado, borrando");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
