package controller;

import com.techu.apitechudb.controllers.HelloController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HelloControllerTest {

    @Test
    public void testHello(){

        HelloController sut = new HelloController();

        String test = sut.hello("Jorge");

        Assertions.assertEquals("Hola Controller: Jorge", sut.hello("Jorge"));

    }
}
